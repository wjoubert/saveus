﻿//import "@aspnet/signalr";

document.addEventListener('DOMContentLoaded', function () {


    const connection = new signalR.HubConnectionBuilder()
        .withUrl('/chat')
        .build();
    bindConnectionMessage(connection);
    connection.start()
        .then(() => onConnected(connection))
        .catch(error => console.error(error.message));
});

function bindConnectionMessage(connection) {
    var messageCallback = function (name, message) {
        if (!message) return;
        var encodedName = name;
        var encodedMsg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
        if (encodedMsg.includes(":")) {
            var result = encodedMsg.split(":");
            addMarker(result[1],result[2],result[0],"Beacon Activated",result[0]);
        }
        
        //alert(encodedName + " : " + encodedMsg);
        
        
    };
    connection.on('broadcastMessage', messageCallback);
    connection.onclose(onConnectionError);
}

function onConnected(connection) {
    console.log('connection started');
    alert('Connected');

}

function onConnectionError(error) {
    if (error && error.message) {
        console.error(error.message);
    }
    var modal = document.getElementById('myModal');
    modal.classList.add('in');
    modal.style = 'display: block;';
}

var infobox = null;
var map = null;

function addMarker(latitude, longitude, title, description, pid) {
    var marker = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(latitude, longitude), { color: 'green' });

    infobox = new Microsoft.Maps.Infobox(marker.getLocation(), {
        visible: false
    });

    marker.metadata = {
        id: pid,
        title: title,
        description: description
    };

    Microsoft.Maps.Events.addHandler(marker, 'mouseout', hideInfobox);
    Microsoft.Maps.Events.addHandler(marker, 'mouseover', showInfobox);

    infobox.setMap(map);
    map.entities.push(marker);
    marker.setOptions({ enableHoverStyle: true });
}

function showInfobox(e) {
    if (e.target.metadata) {
        infobox.setOptions({
            location: e.target.getLocation(),
            title: e.target.metadata.title,
            description: e.target.metadata.description,
            visible: true
        });
    }
}

function hideInfobox(e) {
    infobox.setOptions({ visible: false });
}

