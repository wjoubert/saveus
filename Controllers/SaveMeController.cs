﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SaveUs.Data;
using SaveUs.Data.Monitor;
using SaveUs.Models.Monitor;

namespace SaveUs.Controllers
{
    public class SaveMeController : Controller
    {
        public readonly ApplicationDbContext dbContext;

        public SaveMeController(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Register()
        {
            var userId = GetUserId();
            var beacon = dbContext.Beacons.Where(x => x.IsAssigned == false);
            var model = new RegisterBeaconInputModel(User.Identity.Name,beacon.FirstOrDefault().BeaconId);
            var userBeacon = await dbContext.UserBeacons.FirstOrDefaultAsync(x => x.UserId == userId);
            if(userBeacon != null)
            {
                model = new RegisterBeaconInputModel(User.Identity.Name, userBeacon.Name, userBeacon.Surname, userBeacon.BeaconId, userBeacon.IsActive);
            }
            
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterBeaconInputModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userId = GetUserId();
                    var userBeacon = new UserBeacon(userId, model.Name, model.Surname, model.BeaconId, true);
                    dbContext.UserBeacons.Add(userBeacon);
                    await dbContext.SaveChangesAsync();
                    return RedirectToAction("Register", "SaveMe");
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Register", "SaveMe");
                }

            }
            return View();
        }

        private string GetUserId()
        {
            return User.FindFirstValue(ClaimTypes.NameIdentifier);
        }
    }
}
