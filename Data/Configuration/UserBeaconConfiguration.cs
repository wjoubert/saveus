﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SaveUs.Data.Monitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaveUs.Data.Configuration
{
    public class UserBeaconConfiguration : IEntityTypeConfiguration<UserBeacon>
    {
        public void Configure(EntityTypeBuilder<UserBeacon> builder)
        {
            builder.ToTable("UserBeacon", "Monitor");
            builder.HasKey(x => new { x.UserId, x.BeaconId });
            
        }
    }
}
