﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SaveUs.Data.Monitor;

namespace SaveUs.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<UserBeacon>().ToTable("UserBeacon", "Monitor");
            builder.Entity<UserBeacon>().HasKey(x => new { x.UserId, x.BeaconId });

            builder.Entity<UserBeaconActivation>().ToTable("UserBeaconActivations", "Monitor");
            builder.Entity<UserBeaconActivation>().HasKey(x => new { x.UserBeaconActivationId}).IsClustered();
            builder.Entity<UserBeaconActivation>().HasOne(x => x.UserBeacon).WithMany(x => x.UserBeaconActivations);

            builder.Entity<Beacon>().ToTable("Beacons", "Monitor");
            builder.Entity<Beacon>().HasKey(x => new { x.BeaconId });
            builder.Entity<Beacon>().HasOne(x => x.UserBeacon).WithOne(x=> x.Beacon);
            builder.Entity<Beacon>().HasData(

                new Beacon { BeaconId = "09feee16-8284-4e45-833e-761cdd796545", IsAssigned = false },
                new Beacon { BeaconId = "09fe45tg-8284-4e45-833e-761cdd796545", IsAssigned = false },
                new Beacon { BeaconId = "07deee16-8284-4e45-833e-761cdd796545", IsAssigned = false },
                new Beacon { BeaconId = "07deee16-6584-4e45-833e-761cdd796545", IsAssigned = false },
                new Beacon { BeaconId = "07deee16-8987-4e45-833e-761cdd796545", IsAssigned = false },
                new Beacon { BeaconId = "07deee16-2354-4e45-833e-761cdd796545", IsAssigned = false });
        }

        
        public DbSet<UserBeacon> UserBeacons { get; set; }
        public DbSet<UserBeaconActivation> UserBeaconActivations { get; set; }
        public DbSet<Beacon> Beacons { get; set; }

    }
}
