﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SaveUs.Data.Monitor
{
    public class UserBeaconActivation
    {
        [Key]
        public int UserBeaconActivationId { get; set; }

        [ForeignKey("UserID")]
        public string UserId { get; set; }
        [ForeignKey("BeaconId")]
        public string BeaconId { get; set; }
        public DateTime DateActivated { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public virtual IdentityUser User { get; set; }
        public virtual UserBeacon UserBeacon { get; set; }

    }
}
