﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SaveUs.Data.Monitor
{
    public class UserBeacon 
    {
        [ForeignKey("UserID")]
        public string UserId { get; set; }
        public string BeaconId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public bool IsActive { get; set; }
        
        public virtual IdentityUser User { get; set; }
        public virtual Beacon Beacon { get; set; }
        public virtual List<UserBeaconActivation> UserBeaconActivations { get; set; }

        public UserBeacon(string userId, string name, string surname,string beaconId, bool isActive)
        {
            this.UserId = userId;
            this.Name = name;
            this.Surname = surname;
            this.BeaconId = beaconId;
            this.IsActive = isActive;
        }
    }
}
