﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaveUs.Data.Monitor
{
    public class Beacon
    {
        public string BeaconId { get; set; }
        public bool IsAssigned { get; set; }

        public virtual UserBeacon UserBeacon { get; set; }
       

    }
}
