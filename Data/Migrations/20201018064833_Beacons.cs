﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SaveUs.Data.Migrations
{
    public partial class Beacons : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Beacons",
                schema: "Monitor",
                columns: table => new
                {
                    BeaconId = table.Column<string>(nullable: false),
                    IsAssigned = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Beacons", x => x.BeaconId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserBeacon_BeaconId",
                schema: "Monitor",
                table: "UserBeacon",
                column: "BeaconId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_UserBeacon_Beacons_BeaconId",
                schema: "Monitor",
                table: "UserBeacon",
                column: "BeaconId",
                principalSchema: "Monitor",
                principalTable: "Beacons",
                principalColumn: "BeaconId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserBeacon_Beacons_BeaconId",
                schema: "Monitor",
                table: "UserBeacon");

            migrationBuilder.DropTable(
                name: "Beacons",
                schema: "Monitor");

            migrationBuilder.DropIndex(
                name: "IX_UserBeacon_BeaconId",
                schema: "Monitor",
                table: "UserBeacon");
        }
    }
}
