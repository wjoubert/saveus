﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SaveUs.Data.Migrations
{
    public partial class beaconSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "Monitor",
                table: "Beacons",
                columns: new[] { "BeaconId", "IsAssigned" },
                values: new object[,]
                {
                    { "09feee16-8284-4e45-833e-761cdd796545", false },
                    { "09fe45tg-8284-4e45-833e-761cdd796545", false },
                    { "07deee16-8284-4e45-833e-761cdd796545", false },
                    { "07deee16-6584-4e45-833e-761cdd796545", false },
                    { "07deee16-8987-4e45-833e-761cdd796545", false },
                    { "07deee16-2354-4e45-833e-761cdd796545", false }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "Monitor",
                table: "Beacons",
                keyColumn: "BeaconId",
                keyValue: "07deee16-2354-4e45-833e-761cdd796545");

            migrationBuilder.DeleteData(
                schema: "Monitor",
                table: "Beacons",
                keyColumn: "BeaconId",
                keyValue: "07deee16-6584-4e45-833e-761cdd796545");

            migrationBuilder.DeleteData(
                schema: "Monitor",
                table: "Beacons",
                keyColumn: "BeaconId",
                keyValue: "07deee16-8284-4e45-833e-761cdd796545");

            migrationBuilder.DeleteData(
                schema: "Monitor",
                table: "Beacons",
                keyColumn: "BeaconId",
                keyValue: "07deee16-8987-4e45-833e-761cdd796545");

            migrationBuilder.DeleteData(
                schema: "Monitor",
                table: "Beacons",
                keyColumn: "BeaconId",
                keyValue: "09fe45tg-8284-4e45-833e-761cdd796545");

            migrationBuilder.DeleteData(
                schema: "Monitor",
                table: "Beacons",
                keyColumn: "BeaconId",
                keyValue: "09feee16-8284-4e45-833e-761cdd796545");
        }
    }
}
