﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SaveUs.Data.Migrations
{
    public partial class BeaconForm : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Monitor");

            migrationBuilder.CreateTable(
                name: "UserBeacon",
                schema: "Monitor",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    BeaconId = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Surname = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserBeacon", x => new { x.UserId, x.BeaconId });
                    table.ForeignKey(
                        name: "FK_UserBeacon_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserBeacon",
                schema: "Monitor");
        }
    }
}
