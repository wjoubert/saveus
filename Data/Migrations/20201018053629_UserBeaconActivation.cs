﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SaveUs.Data.Migrations
{
    public partial class UserBeaconActivation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UserBeaconActivations",
                schema: "Monitor",
                columns: table => new
                {
                    UserBeaconActivationId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(nullable: true),
                    BeaconId = table.Column<string>(nullable: true),
                    DateActivated = table.Column<DateTime>(nullable: false),
                    Latitude = table.Column<string>(nullable: true),
                    Longitude = table.Column<string>(nullable: true),
                    UserBeaconUserId = table.Column<string>(nullable: true),
                    UserBeaconBeaconId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserBeaconActivations", x => x.UserBeaconActivationId)
                        .Annotation("SqlServer:Clustered", true);
                    table.ForeignKey(
                        name: "FK_UserBeaconActivations_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserBeaconActivations_UserBeacon_UserBeaconUserId_UserBeaconBeaconId",
                        columns: x => new { x.UserBeaconUserId, x.UserBeaconBeaconId },
                        principalSchema: "Monitor",
                        principalTable: "UserBeacon",
                        principalColumns: new[] { "UserId", "BeaconId" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserBeaconActivations_UserId",
                schema: "Monitor",
                table: "UserBeaconActivations",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserBeaconActivations_UserBeaconUserId_UserBeaconBeaconId",
                schema: "Monitor",
                table: "UserBeaconActivations",
                columns: new[] { "UserBeaconUserId", "UserBeaconBeaconId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserBeaconActivations",
                schema: "Monitor");
        }
    }
}
