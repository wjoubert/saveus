﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SaveUs.Models.Monitor
{
    public class RegisterBeaconInputModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        public string BeaconId { get; set; }

        public bool IsActive { get; set; }

        public RegisterBeaconInputModel()
        {

        }

        public RegisterBeaconInputModel(string email, string name, string surname, string beaconId, bool isActive)
        {
            this.Email = email;
            this.Name = name;
            this.Surname = surname;
            this.BeaconId = beaconId;
            this.IsActive = isActive;
        }

        public RegisterBeaconInputModel(string email,string beaconId)
        {
            this.Email = email;
            this.BeaconId = beaconId;
            this.IsActive = true;
        }
    }
}
